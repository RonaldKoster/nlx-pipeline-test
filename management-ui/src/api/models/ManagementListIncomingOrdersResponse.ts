/* tslint:disable */
/* eslint-disable */
/**
 * management.proto
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: version not set
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ManagementIncomingOrder,
    ManagementIncomingOrderFromJSON,
    ManagementIncomingOrderFromJSONTyped,
    ManagementIncomingOrderToJSON,
} from './';

/**
 * 
 * @export
 * @interface ManagementListIncomingOrdersResponse
 */
export interface ManagementListIncomingOrdersResponse {
    /**
     * 
     * @type {Array<ManagementIncomingOrder>}
     * @memberof ManagementListIncomingOrdersResponse
     */
    orders?: Array<ManagementIncomingOrder>;
}

export function ManagementListIncomingOrdersResponseFromJSON(json: any): ManagementListIncomingOrdersResponse {
    return ManagementListIncomingOrdersResponseFromJSONTyped(json, false);
}

export function ManagementListIncomingOrdersResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ManagementListIncomingOrdersResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'orders': !exists(json, 'orders') ? undefined : ((json['orders'] as Array<any>).map(ManagementIncomingOrderFromJSON)),
    };
}

export function ManagementListIncomingOrdersResponseToJSON(value?: ManagementListIncomingOrdersResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'orders': value.orders === undefined ? undefined : ((value.orders as Array<any>).map(ManagementIncomingOrderToJSON)),
    };
}


